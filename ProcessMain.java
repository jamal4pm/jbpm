package com.sample;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jbpm.test.JBPMHelper;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.RuntimeEnvironmentBuilder;
import org.kie.api.runtime.manager.RuntimeManager;
import org.kie.api.runtime.manager.RuntimeManagerFactory;

import bitronix.tm.resource.jdbc.PoolingDataSource;

public class ProcessMain {

	public static void main(String[] args) {
		KieServices ks = KieServices.Factory.get();
		KieContainer kContainer = ks.getKieClasspathContainer();
		KieBase kbase = kContainer.getKieBase("kbase");
		//setupDataSource();
		RuntimeManager manager = createRuntimeManager(kbase);
		RuntimeEngine engine = manager.getRuntimeEngine(null);
		KieSession ksession = engine.getKieSession();
		ksession.startProcess("com.sample.bpmn.hello");

		manager.disposeRuntimeEngine(engine);
		System.exit(0);
	}

	private static RuntimeManager createRuntimeManager(KieBase kbase) {
		JBPMHelper.startH2Server();
		JBPMHelper.setupDataSource();
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.jbpm.persistence.jpa");
		RuntimeEnvironmentBuilder builder = RuntimeEnvironmentBuilder.Factory.get()
			.newDefaultBuilder().entityManagerFactory(emf)
			.knowledgeBase(kbase);
		return RuntimeManagerFactory.Factory.get()
			.newSingletonRuntimeManager(builder.get(), "com.sample:example:1.0");
	}
	

	public static PoolingDataSource setupDataSource() {
		PoolingDataSource ds1 = new PoolingDataSource();
		ds1.setUniqueName("jdbc/jbpm-ds");
        ds1.setClassName("oracle.jdbc.xa.client.OracleXADataSource");
        ds1.setMaxPoolSize( 5 );
        //ds1.setMinPoolSize(1);
        ds1.setAllowLocalTransactions( true );
        
        ds1.getDriverProperties().put("user","sa");
        ds1.getDriverProperties().put("password","sa");
        ds1.getDriverProperties().put("URL","jdbc:oracle:thin:@172.25.180.112:1521:CLSDEV11");
        ds1.init();
        return ds1;
		
	}
	

}